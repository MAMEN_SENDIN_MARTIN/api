var express = require('express');
var bodyParser = require ('body-parser')
var app = express();
app.use(bodyParser.json())
var cors = require('cors')

var requestJson =require ('request-json')

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancomse/collections"
var apiKey = "apiKey=ji18FLP6u_rVYXMetkn3TcRrsoTgiyqJ"
var clienteMlab

app.get('/apitechu/v5/usuarios', function(req,res){
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?"+ apiKey)

  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      res.send(body)
    }
  })

})


app.get('/apitechu/v5/usuarios/:id', function(req,res){
  var id = req.params.id
  var query = 'q={"id":' + id + '}'
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)

  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      if (body.length >0)
      res.send(body[0])
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})


// sacar con el id el nombre y el apellido

app.get('/apitechu/v5/usuarios/nombre/:id', function(req,res){
  var id = req.params.id
  var query = 'q={"id":' + id + '}'
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)

  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      if (body.length >0)
      res.send( {"nombre":body[0].nombre, "apellido":body[0].apellido})
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})

/*otra forma de hacerlo
app.get('/apitechu/v5/usuarios/:id', function(req, res) {
  var id = req.params.id
  //var query = 'q={"id":' + id + '}'
   var query = 'q={"id":' + id + '}&f={"nombre":1, "apellido":1, "_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      if (body.length > 0)
//        res.send({"nombre":body[0].nombre, "apellido":body[0].apellido})
        res.send(body[0])
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})
*/


// Ejercicio de log in y registro de que se ha quedado logado (actualizar datos)

app.post('/apitechu/v5/login', function(req,res){
  var email = req.headers.email
  var pasword = req. headers.pasword
  var query = 'q={"email":"' + email + '", "pasword":"' + pasword + '"}'
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)

  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      if (body.length ==1) //log in ok
      {
        clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":true}}'
        clienteMlab.put('?q={"id":'+ body [0].id + '}&' + apiKey, JSON.parse(cambio), function (errP, resP, bodyP){
        res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
      })
    }
      else {
        res.status(404).send('Usuario no encontrado')
      }
    }
  })
})


//ejercicio de log out

app.post('/apitechu/v5/logout', function(req,res){
  var id = req.headers.id
  var query = 'q={"id":' + id + ', "logged":true}'
  clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)
  clienteMlab.get('', function(err, resM, body){
    if (!err) {
      if (body.length ==1) // estaba logado
      {
        clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios")
        var cambio = '{"$set":{"logged":false}}'
        clienteMlab.put('?q={"id":'+ body [0].id + '}&' + apiKey, JSON.parse(cambio), function (errP, resP, bodyP){
        res.send({"logout":"ok", "id":body[0].id})
      })
    }
      else {
        res.status(200).send('Usuario no logado previamente')
      }
    }
  })
})


//dado un id, dame las cuentas y el saldo


app.get('/apitechu/v5/cuentas', function (req, res)
{
var idcliente = req.headers.idcliente
var query = 'q={"idcliente":' + idcliente + '}'
var filter = 'f={"iban":1,"saldo":1,"_id":0}'
clienteMlab = requestJson.createClient (urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
clienteMlab.get('', function(err, resM, body){
  if (!err) {
  res.send(body)
    }
})
})



// dado una cuenta, dame los movimientos:


app.get('/apitechu/v5/movimientos', function(req, res) {
  var iban = req.headers.iban
  var query = 'q={"iban": "' + iban + '"}'
  var filter = 'f={"movimientos":1,"_id":0}'
  clienteMlab = requestJson.createClient(urlMlabRaiz + "/cuentas?" + query + "&" + filter + "&" + apiKey)
  clienteMlab.get('', function(err, resM, bodyP) {
  if (!err) {
      // console.log(clienteMlab)
        res.send(bodyP)
      }
      else {
        res.status(404).send("iban no encontrado")
      }
  })
})


//registro nuevo registro

app.post('/apitechu/v5/registro', function(req, res){

  var email = req.headers.email
  var pasword = req. headers.pasword
  var nombre= req.headers.nombre
  var apellido = req.headers.apellido
  var telefono = req.headers.telefono
  var ciudad = req.headers.ciudad
  var nuevoRegistro = {}

  var clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
    clienteMlab.get ('', function(err, resM, body) {
      if (!err) {
        var totalregistros = body.length
      //  console.log("totalregistros: ", totalregistros)
        var query = 'q={"email":"' + email + '"}'
      //  console.log("-" + urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey + "-");
        clienteMlab = requestJson.createClient (urlMlabRaiz + "/usuarios?" + query + "&l=1&"+ apiKey)
        clienteMlab.get('', function(err, resM, body){
      //    console.log("body tras select: ",body)
        //  console.log("body.length: ", body.length)
          if (!err) {
            if (body.length ==1) //el usuario y pasword ya existen
            {
              res.send ({"Detalle":'Usuario ya existe'})
            }
          else {
            //console.log("entra alta del nuevo registro")
            nuevoRegistro.id = totalregistros + 1
            nuevoRegistro.nombre = nombre
            nuevoRegistro.apellido = apellido
            nuevoRegistro.email = email
            nuevoRegistro.pasword = pasword
            nuevoRegistro.ciudad = ciudad
            nuevoRegistro.telefono = telefono

            clienteMlab = requestJson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
            console.log("nuevoRegistro: ", nuevoRegistro)
            var cambio = JSON.stringify(nuevoRegistro)
          //  console.log("cambio: ", cambio)
            //console.log("clienteMlab: ", clienteMlab)
              clienteMlab.post('', JSON.parse(cambio),function(errP, resP, bodyP){
              console.log("bodyP: ", bodyP)
                      if (!errP) {
                        res.send({"Detalle":"Cliente dado de alta"})
                      }
                      else {
                        res.send({"Detalle":"Error al insertar datos de alta registro"})
                            }
                        })
                }
              }
                    })
                  }
                })
              })


var port = process.env.PORT || 3000;
var fs = require('fs')


app.listen(port);
console.log("API escuchando en el puerto"+ port);


var usuarios = require('./usuarios.json')

console.log("hola mundo");

app.get('/apitechu/v1', function (req, res)
{
//console.log(req)
res.send({"mensaje":"Bienvenido a mi API"})
})

app.get('/apitechu/v1/usuarios', function (req, res)
{
res.send(usuarios)
})

app.post('/apitechu/v1/usuarios', function(req, res)
{
var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country}
usuarios.push(nuevo)
console.log(req.headers)
const datos = JSON.stringify (usuarios)
fs.writeFile ("./usuarios-2.json", datos, "utf8", function(err) {
if (err){
  console.log (err)
}
else {
    console.log ("Fichero guardado")
    res.send("Alta ok")
}
})
res.send("Alta ok")
})


app.post('/apitechu/v2/usuarios', function(req, res)
{
//var nuevo = {"first_name":req.headers.first_name, "country":req.headers.country}
var nuevo = req.body
usuarios.push(nuevo)
//console.log(req.headers)
const datos = JSON.stringify (usuarios)
fs.writeFile ("./usuarios-2.json", datos, "utf8", function(err) {
if (err){
  console.log (err)
}
else {
    console.log ("Fichero guardado")
    res.send("Alta ok")
}
})
res.send("Alta ok")
})




app.delete('/apitechu/v1/usuarios/:elegido', function require(req, res){
  usuarios.splice (req.params.elegido-1,1)
  res.send("usuario borrado")
})


app.post('/apitechu/v1/monstruo/:p1/:p2', function(req,res){
  console.log ("Parámetros")
  console.log(req.params)
  console.log("Query Strings")
  console.log (req.query)
  console.log (req.headers)
  console.log("Body")
  console.log(req.body)
  res.send("se ha procesado todo correctamente")
})





app.post('/apitechu/v3/usuarios/login', function(req, res){
  var email = req.headers.email
  var password = req.headers.password
  var idusuario =0

  console.log(usuarios.length)

  for (var i = 0; i < usuarios.length; i++) {

    if(usuarios[i].email == email && usuarios[i].pasword == password)
    {
    idusuario = usuarios[i].id
    usuarios[i].logged = true
    break;
    }
  }
if (idusuario !=0) //encontrado
  res.send({"encontrado":"si","id":idusuario})
  else {
    res.send( {"encontrado":"no"})
  }

})



app.post('/apitechu/v3/usuarios/logout', function(req, res){
  var idusuario = req.headers.id
  var usuario_logado = false

  for (var i = 0; i < usuarios.length; i++) {

    if(usuarios[i].id == idusuario && usuarios[i].logged == true)
    {
      usuario_logado = true
      usuarios[i].logged = false
    break;
    }
  }

  if (usuario_logado==true)
    res.send({"logout":"si", "id":idusuario})
  else
    res.send({"logout":"no", "msj":"el usuario no tiene la sesión iniciada"})
})


//BLOQUE CUENTAS

//ejercicio 1: lista de cuentas que hay

var cuentas = require ('./cuentas.json')

app.get('/apitechu/v1/cuentas', function (req, res)
{
res.send(cuentas)
})


// ejercicio 1: lista de cuentas que hay

app.get('/apitechu/v1/cuentas/iban', function (req, res)
{
  var ibanarray = []
  for (var i = 0; i < cuentas.length; i++){
    ibanarray.push(cuentas[i].iban)
}
res.send(ibanarray)
})

//ejercicio 2: dado un número de cuenta, dame su movimiento



app.get('/apitechu/v1/cuentas/movimiento', function (req, res)
{
var iban = req.headers.iban
var movimientosarray = []

  for (var i = 0; i < cuentas.length; i++){

    if(cuentas[i].iban == iban)
    {
  movimientosarray.push(cuentas[i].movimientos)
    }
}
res.send(movimientosarray)
})


//ejercicio 3: dado un usuario con un id , dame las cuentas

app.get('/apitechu/v1/cuentas/usuario', function (req, res)
{
var idcliente = req.headers.idcliente
var ibanarray = []

  for (var i = 0; i < cuentas.length; i++){

    if(cuentas[i].idcliente == idcliente)
    {
  ibanarray.push(cuentas[i].iban)
    }
}
res.send(ibanarray)
})
